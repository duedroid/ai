class City:

    def __init__(self, name, romania, XPos, YPos):
        self.name = name
        self.XPos = XPos - (920/2)
        self.YPos = (560/2) - YPos
        self.path = {}
        self.displacement = {}
        romania.add_city(self)

    def set_path(self, path_list):
        for obj in path_list:
            self.path.update(obj)
        print(self.path)
    # def set_displacement(self, displacement_list):
    #     for obj in displacement_list:
    #         self.displacement.update({
    #             city: city,
    #             displacement: displacement
    #         })

    def __str__(self):
        return self.name


class Romania:

    def __init__(self):
        self.city_list = []

    def add_city(self, city):
        self.city_list.append(city)

    def get_path(self, city1, city2):
        pass

    def __str__(self):
        result = ""
        for city in self.city_list:
            result += city.name + " "
        return result


romania = Romania()
arad = City("Arad", romania, 64, 161)
arad.set_path([{'zerind':75}, {'timisoara':118}, {'sibiu':140}])

bucharest = City("Bucharest", romania, 600, 444)
ิีbucharest.set_path([{'fagaras':211}, {'pitesti':101}, {'giurgiu':90}, {'urziceni':85}])

craiova = City("Craiova", romania, 344, 513)
craiova.set_path({'drobeta':120}, {'pitesti':138}, {'rimnicu':146})

drobeta = City("Drobeta", romania, 193, 493)
eforie = City("Eforie", romania, 878, 503)
fagaras = City("Fagaras", romania, 437, 234)
giurgiu = City("Giurgiu", romania, 555, 542)
hirsova = City("Hirsova", romania, 830, 405)
iasi = City("Iasi", romania, 725, 135)
lugoj = City("Lugoj", romania, 193, 356)
mehadia = City("Mehadia", romania, 198, 424)
neamt = City("Neamt", romania, 609, 83)
oradea = City("Oradea", romania, 134, 23)
pitesti = City("Pitesti", romania, 462, 376)
rimnicu = City("Rimnicu", romania, 310, 301)
sibiu = City("Sibiu", romania, 267, 219)
timisoara = City("Timisoara", romania, 70, 302)
urziceni = City("Urziceni", romania, 697, 405)
vaslui = City("Vaslui", romania, 787, 242)
zerind = City("Zerind", romania, 95, 92)
print(arad)
